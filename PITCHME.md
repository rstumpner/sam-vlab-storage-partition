---
marp: true
---
## Systemadministraton 2 Übung
* SAM 2 Übung
* Storage


---
## Partitionierung
* Storage Partitionierung
* Version 20190402

---
## Agenda Partitionierung
* Voraussetzungen vLAB
* Installation
* Einführung
* Umgebung vLAB
* First Steps
* Aufgaben
* Troubleshooting
* Cheatsheet
* Links
* Notizen

---
## Voraussetzungen vLAB
* Hypervisor Virtualbox / HyperV
* 1 x CPU Core
* 1 GB RAM
* 2 x Disks mininum 1 GB
* Linux Ubuntu 18.04 LTS

---
## Vorbereitungen vLAB
* Vorbereitungen vLAB Vagrant
* Vorbereitungen vLAB Manual (optional)

---
## Vorbereitungen vLAB Vagrant
* git clone https://gitlab.com/rstumpner/sam-vlab-storage-partition.git
* cd vlab/vagrant-virtualbox
* vagrant up
* vagrant ssh
* Username: vagrant
* Password: vagrant

---
## Vorbereitungen vLAB Manual 
#### optional
* VM Erstellen
* 1 CPU
* 2 GB RAM
* 2 GB Disk für Betriebssystem
* Hinzufügen einer Disk 1 GB
* Ubuntu 18.04 LTS Basisinstallation

---
## Partitionierung Einführung

Bevor ein Storage Medium mit Daten befüllt werden kann muss eine Struktur (Container) für die Daten festgelegt werden diesen Vorgang nennt man Partitionierung.

---
## Partitionstypen
* Master Boot Record
* GUID Partition Table

---
## Master Boot Record
![Master Boot Record](_images/partition-mbr.png)

---
## GUID Partition Table
![GPT](_images/partition-gpt.png)

---
## First Steps
* Überprüfen der Umgebung
* Erstellen einer Partition
* Erstellen eines Dateisystems
* Einhängen eines Dateisystems

---
## Überprüfen der Umgebung
* Überprüfen der Speicherumgebung
`df -h`
* Überprüfen der Einhängepunkte
`mount`
* Überprüfen der Blockspeicher
`lsblk`

---
## Überprüfen einer Partition

```
cfdisk /dev/sda

Label: ???
Identifier: ???
Type: ???
Filesystem: ???
Mountpoint: ???
```

---
## Erstellen einer Partition

```
cfdisk /dev/sdb

Label: gpt
Type: 83
Identifier: ???
Filesystem: ???
Mountpoint: ???
```
---
## Überprüfen der Partition

`lsblk`

---
## Erstellen eines Dateisystems

`mkfs.ext4 /dev/sdb1`

Überprüfen:

`lsblk`

---
## Einhängen eines Dateisystems

`sudo mount /dev/sdb1 /media/`

* Check

` df -h`



---
## Aufgaben Level 1

* Datei erstellen (dummy)
* Disk größe Erweitern
* Partitionsgröße Erweitern
* Dateisystemgröße Erweitern
* Ist die Datei noch lesbar und kein Bitrot

---
## Partitionierung Lösungsansätze

---
## Partitionierung Cheat Sheet
* List Blockdevices (lsblk)
* Starte Partitionierung (cfdisk /dev/sdX)

---
## Links
* Gparted ist auch als seperate Boot Disk / USB Stick erhältlich http://gparted.org/livecd.php

---
## Notizen
![lsblk](_images/partition-lsblk.png)

---
![lsblk](_images/partition-fdisk.png)

---
![lsblk](_images/partition-cfdisk.png)

---
![lsblk](_images/partition-gparted.png)

---
![lsblk](_images/partition-sgdisk.png)

---
![lsblk](_images/partition-typen.png)
