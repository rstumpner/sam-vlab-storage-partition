<!-- $size: 16:9 -->
<!-- page_number: true -->
<!-- footer: Roland Stumpner GPLv3 -->


# Systemadministraton 2 Übung
* SAM 2 Übung
* Storage


---
# Partitionierung
* Storage Partitionierung
* Version 20171327

---
# Partitionierung Übung Übersicht
* Partitionierung Voraussetzungen vLAB
* Partitionierung Einführung
* Partitionierung First Steps
* Partitionierung Aufgaben
---
# Partitionierung Brainstorming
---
# Partitionierung Voraussetzungen vLAB
* VM ( Virtualbox / Vmware Workstation / Proxmox VM)
* 1 x CPU Core
* 2 GB RAM
* 2 x VM Disks mininum 2 GB
* Linux Ubuntu 16.04 LTS
---
# Partitionierung Einführung
* Bevor ein Storage Medium mit Daten befüllt werden kann muss eine Struktur (Container) für die Daten festgelegt werden diesen Vorgang nennt man Partitionierung.
---
# Master Boot Record System
![Master Boot Record](partition-mbr.png)

---
# GUID Partition Table
![GPT](partition-gpt.png)

---
# Partitionierung Einführung Installation (Manual)
* Ubuntu 16.04 LTS Basis
---
# Partitionierung with vLAB (Vagrant)
* git pull https://www.github.com/stumpi/vLab/sam-vlab-storage-partitionierung.git
---
# First Steps in Partitionierung von Speichern
* Linux VM erstellen
* Linux VM Installieren
* Hinzufügen einer neuen Disk
* Partitionierung einer neuen Disk
   `cfdisk /dev/sdX`
---
# Partitionierung Aufgaben
* Formatierung mit einem Dateisystem
* Dummy Datei erstellen
* Disk größe Erweitern
* Partitionsgröße Erweitern
* Datasystemgröße Erweitern
* Ist die Datei noch lesbar und kein Bitrot
---
# Partitionierung Lösungsansätze
---
# Partitionierung Cheat Sheet
* List Blockdevices (lsblk)
* Starte Partitionierung (cfdisk /dev/sdX)
---
# Links
* Gparted ist auch als seperate Boot Disk / USB Stick erhältlich http://gparted.org/livecd.php
---
# Notizen
![lsblk](partition-lsblk.png)

---
![lsblk](partition-fdisk.png)

---
![lsblk](partition-cfdisk.png)

---
![lsblk](partition-gparted.png)

---
![lsblk](partition-sgdisk.png)

---
![lsblk](partition-typen.png)